use v5.8;


package Rectangle {
    use Moose;

    has width  => ( is => 'ro' );
    has height => ( is => 'ro' );

    sub area {
        my $self = shift;
        return $self->width * $self->height;
    }
}

my $r = Rectangle->new( width => 2, height => 3 );
printf "%d x %d = %d\n",
    $r->width, $r->height, $r->area;
