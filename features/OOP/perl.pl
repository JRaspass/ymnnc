use v5.40;
use experimental 'class';

class Rectangle {
    field $width  :param :reader;
    field $height :param :reader;

    method area {
        return $width * $height;
    }
}




my $r = Rectangle->new( width => 2, height => 3 );
printf "%d x %d = %d\n",
    $r->width, $r->height, $r->area;
