use v5.8;
use List::MoreUtils 'natatime';

my @foo;

my $it = natatime 2, @foo;
while (my ($bar, $baz) = $it->()) {
    ...
}
