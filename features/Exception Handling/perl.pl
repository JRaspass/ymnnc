use v5.36;
use experimental 'try';

try {
    ...
}
catch ($e) {
    warn "Caught: $e";
}
finally {
    say 'Finished';
}
