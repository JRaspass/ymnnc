use v5.6;
use Try::Tiny;

try {
    ...
}
catch {
    warn "Caught: $_";
}
finally {
    print "Finished\n";
};
