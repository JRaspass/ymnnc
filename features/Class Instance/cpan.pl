use v5.6;
use Safe::Isa;

my $foo = Foo->new;

if ( $foo->$_isa('Bar::Baz') ) {
    ...
}
